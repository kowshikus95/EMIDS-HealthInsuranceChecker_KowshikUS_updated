package com.project.hig;

import com.project.hig.exceptions.CustomException;
import com.project.hig.model.PatientInput;

public interface HealthInsuranceCheckerService {
	

	public Float getPatientInsuranceAmt(PatientInput patient) throws CustomException ;

	public Float getPatientPremiumAmt(PatientInput patient) throws CustomException ;

}
