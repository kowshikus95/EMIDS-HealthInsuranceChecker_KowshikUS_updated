package com.project.hig;

import com.project.hig.constants.AppConstants;
import com.project.hig.enums.GenderEnum;
import com.project.hig.exceptions.CustomException;
import com.project.hig.model.CurrentHabit;
import com.project.hig.model.CurrentHealth;
import com.project.hig.model.PatientInput;
import com.project.hig.util.AppUtil;

public class HealthInsuranceCheckerServiceImpl implements HealthInsuranceCheckerService{
	public Float getPatientInsuranceAmt(PatientInput patient) throws CustomException {	
		if(patient==null||patient.getDob()==null) {
			throw new CustomException("Invalid Patient");
		}

		int patientAge = AppUtil.computeAge(patient.getDob());
		
		float agePremiumAmount = HealthInsuranceCalcUtil.getAgePremiumAmount(patientAge,AppConstants.BASE_PREMIUM_AMT);

		if(patient.getGender().equals(GenderEnum.MALE.getDisplayText())) {
			agePremiumAmount+=agePremiumAmount*AppConstants.MALE_PREMIUM_SLAB_PERCENT;
		}
		
		for(CurrentHealth health : patient.getCurrentHealthStack()) {
			if(health.getPresent())
				agePremiumAmount+=agePremiumAmount*AppConstants.PRECONDITON_PERCENT;
		}
		
		for(CurrentHabit habit : patient.getCurrentHabitStack()) {
			if(habit.getBadHabit() && habit.getPresent())
				agePremiumAmount+=agePremiumAmount*AppConstants.BAD_HABIT_INC_PERCENT;
			if(!habit.getBadHabit() && habit.getPresent()) {
				agePremiumAmount-=agePremiumAmount*AppConstants.GOOD_HABIT_DEC_PERCENT;
			}	
		}
		
		System.out.println("Health insurance premium of patient["+patient.getName()+"] is "+agePremiumAmount);
		return agePremiumAmount;
		
	}

	@Override
	public Float getPatientPremiumAmt(PatientInput patient) throws CustomException {
		// TODO Auto-generated method stub
		return null;
	}


}
