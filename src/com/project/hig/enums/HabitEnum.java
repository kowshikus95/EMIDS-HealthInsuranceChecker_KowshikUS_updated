package com.project.hig.enums;

public enum HabitEnum {

	EXERCISE(1,"Daily exercise",false),SMOKING(2,"Smoking",true),ALCOHOL(3,"Consumption of alcohol",true),DRUGS(4,"Drugs",true);
	
	private int index;
	private String displayText;
	private boolean badHabit;
	
	HabitEnum(int idx,String display,boolean bad){
		index= idx;
		displayText= display;
		badHabit = bad;
	}

	public int getIndex() {
		return index;
	}

	public String getDisplayText() {
		return displayText;
	}


	public boolean isBadHabit() {
		return badHabit;
	}

	
	
}
