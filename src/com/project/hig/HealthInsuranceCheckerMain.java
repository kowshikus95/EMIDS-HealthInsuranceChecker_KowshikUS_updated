package com.project.hig;

import com.project.hig.exceptions.CustomException;
import com.project.hig.mocks.PatientMocks;

public class HealthInsuranceCheckerMain {

	public static void main(String[] args) {
		HealthInsuranceCheckerService healthInsuranceCheckerService = new HealthInsuranceCheckerServiceImpl();
		try {
			healthInsuranceCheckerService.getPatientInsuranceAmt(PatientMocks.getAboveAgeMalePatientExceedsAllLimits());
			healthInsuranceCheckerService.getPatientInsuranceAmt(PatientMocks.getNormalUnderAgeNonMalePatient());
			
		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
