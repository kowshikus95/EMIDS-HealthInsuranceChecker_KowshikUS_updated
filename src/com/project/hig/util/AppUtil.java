package com.project.hig.util;

import java.util.Date;

public class AppUtil {

	public static int computeAge(Date dob)
	{
	    long timeDiff = System.currentTimeMillis() - dob.getTime();      
	    int age = (int)(timeDiff / 31558464000L);
	    return age; 
	}
}
