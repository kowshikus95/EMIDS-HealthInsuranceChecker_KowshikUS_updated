package com.project.hig;

public class HealthInsuranceCalcUtil {

	public static float getAgePremiumAmount(Integer age,Integer baseAmt) {
		//% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
		float result=baseAmt;
		if(age>=18)result+=(result*0.1);
		if(age>=25)result+=(result*0.1);
		if(age>=30)result+=(result*0.1);
		if(age>=35)result+=(result*0.1);
		if(age>=40)result+=(result*0.2);
		return result;	
	}
	
}
