package com.project.hig.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.project.hig.HealthInsuranceCheckerService;
import com.project.hig.HealthInsuranceCheckerServiceImpl;
import com.project.hig.exceptions.CustomException;
import com.project.hig.mocks.PatientMocks;
import com.project.hig.model.PatientInput;

public class HealthInsuranceTests {

	private HealthInsuranceCheckerService healthInsuranceCheckerService = new HealthInsuranceCheckerServiceImpl();
		
	@Test
	public void testNormalUnderAgedNonMalePatientFailCase() throws CustomException {
		PatientInput normalUnderAgeNonMalePatient = PatientMocks.getNormalUnderAgeNonMalePatient();
		Float patientInsuranceAmt = healthInsuranceCheckerService.getPatientInsuranceAmt(normalUnderAgeNonMalePatient);
		assertNotEquals(patientInsuranceAmt, 0);
	}

	@Test
	public void testNormalUnderAgedNonMalePatientSuccessCase() throws CustomException {
		PatientInput normalUnderAgeNonMalePatient = PatientMocks.getNormalUnderAgeNonMalePatient();
		Float patientInsuranceAmt = healthInsuranceCheckerService.getPatientInsuranceAmt(normalUnderAgeNonMalePatient);
		assertEquals(patientInsuranceAmt, MockConstants.NORMAL_PATIENT_AMT);
	}
	@Test
	public void testMalePatientExeceedingAllLimits() throws CustomException {
		PatientInput aboveAgeMaleExceeder = PatientMocks.getAboveAgeMalePatientExceedsAllLimits();
		Float patientInsuranceAmt = healthInsuranceCheckerService.getPatientInsuranceAmt(aboveAgeMaleExceeder);
		assertEquals(patientInsuranceAmt, MockConstants.ABNORMAL_MALE_AGE27_PATIENT_AMT);
	}

	@Test(expected = CustomException.class)
	public void testInvalidPatientCustomException() throws CustomException {
		PatientInput patient = null;
		Float patientInsuranceAmt = healthInsuranceCheckerService.getPatientInsuranceAmt(patient);
		assertNotEquals(patientInsuranceAmt, 0);
	}

}
