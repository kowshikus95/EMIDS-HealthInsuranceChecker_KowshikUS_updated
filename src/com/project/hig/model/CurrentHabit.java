package com.project.hig.model;

import com.project.hig.enums.HabitEnum;

public class CurrentHabit {

	private String habitName;
	
	private Boolean present;
	
	private Boolean badHabit;
	
	

	public CurrentHabit(HabitEnum exercise, boolean present) {
		this.habitName = exercise.getDisplayText();
		this.present = present;
		this.badHabit = exercise.isBadHabit();
	}

	public String getHabitName() {
		return habitName;
	}

	public void setHabitName(String habitName) {
		this.habitName = habitName;
	}

	public Boolean getPresent() {
		return present;
	}

	public void setPresent(Boolean present) {
		this.present = present;
	}

	public Boolean getBadHabit() {
		return badHabit;
	}

	public void setBadHabit(Boolean badHabit) {
		this.badHabit = badHabit;
	}
	
	
}
