package com.project.hig.model;

public class CurrentHealth {

	private String healthName;
	
	private Boolean present;

	public CurrentHealth(String healthName, Boolean present) {
		super();
		this.healthName = healthName;
		this.present = present;
	}

	public String getHealthName() {
		return healthName;
	}

	public void setHealthName(String healthName) {
		this.healthName = healthName;
	}

	public Boolean getPresent() {
		return present;
	}

	public void setPresent(Boolean present) {
		this.present = present;
	}
	
	
}
