package com.project.hig.constants;

public class AppConstants {
	
	public final static int BASE_PREMIUM_AMT= 5000;
	
	public final static float MALE_PREMIUM_SLAB_PERCENT = 0.02f;
	
	public final static float PRECONDITON_PERCENT = 0.01f;
	
	public final static float GOOD_HABIT_DEC_PERCENT = 0.03f;
	
	public final static float BAD_HABIT_INC_PERCENT = 0.03f;

}
